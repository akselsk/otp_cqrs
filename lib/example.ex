defmodule Commands do
  defmodule CreateCounter do
    defstruct []
  end

  defmodule Increment do
    defstruct []
  end
end

defmodule Events do
  defmodule CounterCreated do
    @derive Jason.Encoder
    defstruct [:stream_id]
  end

  defmodule Incremented do
    @derive Jason.Encoder
    defstruct [:stream_id]
  end
end

defmodule ReadModels do
  defmodule Counter do
    defstruct [:id, :value]

    def get_instance_id(event), do: event.stream_id

    def apply_event(_counter, %Events.CounterCreated{} = event),
      do: %Counter{id: event.stream_id, value: 0}

    def apply_event(counter, %Events.Incremented{} = event) do
      %Counter{id: event.stream_id, value: counter.value + 1}
    end
  end
end

defmodule Aggregates do
  defmodule Counter do
    defstruct [:value]

    def apply_event(_state, %Events.CounterCreated{}), do: {:ok, %Counter{value: 0}}
    def apply_event(state, %Events.Incremented{}), do: {:ok, %Counter{value: state.value + 1}}
  end
end

defimpl OtpCqrs.Aggregate, for: Commands.CreateCounter do
  def execute(_command, id) do
    apply_event_func = fn state, event -> Aggregates.Counter.apply_event(state, event) end

    execute_command_func = fn state ->
      case state do
        nil -> {:ok, %Events.CounterCreated{stream_id: id}}
        _ -> {:error, :already_exists}
      end
    end

    OtpCqrs.AggregateAgent.execute(id, apply_event_func, execute_command_func)
  end
end

defimpl OtpCqrs.Aggregate, for: Commands.Increment do
  def execute(_command, id) do
    IO.inspect(id)
    apply_event_func = fn state, event -> Aggregates.Counter.apply_event(state, event) end

    execute_command_func = fn state ->
      case state do
        nil -> {:error, :does_not_exist}
        _ -> {:ok, %Events.Incremented{stream_id: id}}
      end
    end

    OtpCqrs.AggregateAgent.execute(id, apply_event_func, execute_command_func)
  end
end
