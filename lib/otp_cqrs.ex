defmodule OtpCqrs do
  @moduledoc """
  Documentation for `OtpCqrs`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> OtpCqrs.hello()
      :world

  """
  def hello do
    :world
  end
end
