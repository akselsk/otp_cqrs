defmodule OtpCqrs.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      {Registry, keys: :unique, name: AggregateRegistry},
      {Registry, keys: :unique, name: ReadModelRegistry},
      {Phoenix.PubSub, name: :read_model_pubsub},
      {DynamicSupervisor, strategy: :one_for_one, name: AggregateSupervisor}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OtpCqrs.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
