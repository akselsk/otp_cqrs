defmodule OtpCqrs.ReadModelAgent do
  use GenServer

  def start_link(args) do
    [read_model: read_model] = args

    GenServer.start_link(__MODULE__, [read_model], name: read_model, id: read_model)
  end

  def init([read_model]) do
    :ok = OtpEs.read_and_subscribe_all_events()
    {:ok, {read_model, %{}, %{}}}
  end

  def get(name, id) do
    GenServer.call(name, {:get, id})
  end

  def subscribe(name, id) do
      Phoenix.PubSub.subscribe(:read_model_pubsub, to_string(name)<> id)
  end

  def unsubscribe(name, id) do
      Phoenix.PubSub.unsubscribe(:read_model_pubsub, to_string(name)<> id)
  end

  def get_and_subscribe(name, id) do
      :ok = subscribe(name, id)
      get(name, id)
  end


  def handle_call({:get, id}, _, {_, _, state} = full_state) do
    {:reply, state[id], full_state}
  end

  def handle_info({stream_id, nr, event}, {read_model, handled_events, state}) do
    update_read_model =
      (fn ->
         read_model.get_instance_id(event)
         |> (fn id ->
             Map.get(state, id)
             |> read_model.apply_event(event)
             |> (fn new_instance ->
                 Phoenix.PubSub.broadcast(:read_model_pubsub, to_string(read_model)<> id, new_instance)
                 Map.put(state, id, new_instance)
                end).()
             end).()
             |> fn new_state ->
             	{:noreply, {read_model, Map.put(handled_events, stream_id, nr), new_state}} end.()
       end)

    new_state =
      case handled_events[stream_id] do
        nil when nr == 1 -> update_read_model.()
        x when x == nr - 1 -> update_read_model.()
        _ -> send(self(), {stream_id, nr, event})
            {:noreply, {read_model, handled_events, state}}
      end

  end
end
