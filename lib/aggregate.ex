defmodule OtpCqrs.AggregateAgent do
  use GenServer

  def start_link(args) do
    [stream_id: stream_id, apply_event_func: apply_event_func, name: name] = args

    GenServer.start_link(__MODULE__, [stream_id, apply_event_func], name: name)
  end

  def execute(stream_id, apply_event_func, execute_command_func) do
    {:ok, pid} = find_or_start_aggregate_agent(stream_id, apply_event_func)
    GenServer.call(pid, {:execute, execute_command_func})
  end

  def init([stream_id, apply_event_func]) do
    GenServer.cast(self(), :finish_init)
    {:ok, {stream_id, apply_event_func, nil, 0}}
  end

  def handle_cast(:finish_init, {stream_id, apply_event_func, nil, 0}) do
    events = OtpEs.get_all_events_from_stream(stream_id)
    state = Enum.reduce(events, nil, fn event, state -> apply_event_func.(state, event) end)
    {:noreply, {stream_id, apply_event_func, state, length(events)}}
  end

  def handle_call(
        {:execute, execute_command_func},
        _from,
        {stream_id, apply_event_func, state, event_nr}
      ) do
    with {:ok, event} <- execute_command_func.(state),
         :ok <- OtpEs.put_event(stream_id, event, event_nr + 1),
         {:ok, new_state} <- apply_event_func.(state, event) do
      {:reply, :ok, {stream_id, apply_event_func, new_state, event_nr + 1}}
    else
      err -> {:reply, err, {stream_id, apply_event_func, state, event_nr}}
    end
  end

  defp find_or_start_aggregate_agent(stream_id, apply_event_func) do
    Registry.lookup(AggregateRegistry, stream_id)
    |> case do
      [{pid, _}] ->
        {:ok, pid}

      [] ->
        spec =
          {__MODULE__,
           stream_id: stream_id,
           apply_event_func: apply_event_func,
           name: {:via, Registry, {AggregateRegistry, stream_id}}}

        DynamicSupervisor.start_child(AggregateSupervisor, spec)
    end
  end
end

defprotocol OtpCqrs.Aggregate do
  def execute(command, aggregate_id)
end
