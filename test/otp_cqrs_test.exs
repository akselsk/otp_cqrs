defmodule OtpCqrsTest do
  use ExUnit.Case
  doctest OtpCqrs

  test "Aggregate and read model" do
    nodes = LocalCluster.start_nodes("my-cluster", 3)
    OtpEs.delete_event("torscounter", 1)
    OtpEs.delete_event("torscounter", 2)
    OtpEs.delete_event("torscounter", 3)
    assert OtpCqrs.Aggregate.execute(%Commands.CreateCounter{}, "torscounter") == :ok
    assert OtpCqrs.Aggregate.execute(%Commands.Increment{}, "torscounter") == :ok

    a = OtpCqrs.ReadModelAgent.start_link([read_model: ReadModels.Counter])
    :timer.sleep(1000)

    assert OtpCqrs.ReadModelAgent.get(ReadModels.Counter, "torscounter") == %ReadModels.Counter{
             id: "torscounter",
             value: 1
           }

    assert OtpCqrs.Aggregate.execute(%Commands.Increment{}, "torscounter") == :ok

    assert OtpCqrs.ReadModelAgent.get(ReadModels.Counter, "torscounter") == %ReadModels.Counter{
             id: "torscounter",
             value: 2
           }

    OtpCqrs.ReadModelAgent.get_and_subscribe(ReadModels.Counter, "torscounter")
    OtpCqrs.Aggregate.execute(%Commands.Increment{}, "torscounter") == :ok
    assert_receive %ReadModels.Counter{id: "torscounter", value: 3}, 3000


    OtpEs.delete_event("torscounter", 1)
    OtpEs.delete_event("torscounter", 2)
    OtpEs.delete_event("torscounter", 3)
    OtpEs.delete_event("torscounter", 4)
  end
end
