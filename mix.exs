defmodule OtpCqrs.MixProject do
  use Mix.Project

  def project do
    [
      app: :otp_cqrs,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      aliases: [
        test: "test --no-start"
      ],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {OtpCqrs.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:otp_es, git: "https://gitlab.com/akselsk/otp_es"},
      {:local_cluster, "~> 1.2", only: [:test]},
      {:phoenix_pubsub, "~> 2.0.0"}
    ]
  end
end
