# OtpCqrs

A wrapper around opt_es (gitlab.com/akselsk/opt_es) that enables CQRS (command query response segregation)
By using the strong state handling of the OTP the aggregate state is stored in memory and must not be 
rebuild for each new command.

At the moment the read model is also just in memory instead of a sql db which is more common.
 
## How to use

Read  https://martinfowler.com/bliki/CQRS.html for the general idea
And see example.ex for an example.

1. Define your command, and events, and (def)implement the execute function for your command

2. Define aggregates to validate commands and create events

3. Define read models that listens to events and builds the desired state. Start each 
readmodel by putting them in a supervision tree. See this application.ex for an example.



## How it works

When a command is executed an AggregateAgent is spun up for that stream. It reads all 
event in the stream (from otp_es) and builds up a state.  It then check if the 
command is valid, and if so it creates an event, writes it to the eventstore, and updates
the aggregate state. The AggregateAgent persists so the next command is quicker. 

The read models subscribes to new events, checks if it is relevant, and applies it. The read model 
state is then broadcasted so processes can listen to the derived state. For example: a liveview. 


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `otp_cqrs` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:otp_cqrs, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/otp_cqrs](https://hexdocs.pm/otp_cqrs).

